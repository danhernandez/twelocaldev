# TWE Front End Dev Kit - No Visual Studio Required

I created this as a quicker and more light-weight way of working with CSS on the TWE repo. Handy if you need to make and preview changes on a Mac or outside of a visual studio solution.

#### What's in the box:

* A LESS and SASS compiler
* Automatically reloads your changes in the browser on save
* View your local changes on mobile devices without deploying to server
* Write standard CSS without vendor prefixes
* Easy compilation via [Gulp](Gulp), setup purely via [npm](https://npmjs.com/).

## Setup
1. Install [Node.js](https://nodejs.org/en/).
2. Install [Gulp](http://gulpjs.com/). `npm install -g gulp`
3. Drop this folder / repo in `twe-sitecore-masp` (the top level directory) and run `npm install`.

This is purely for developing locally, and so you'll still need to commit your changes via git.

## Usage
Once installed, run `gulp build:less` or `gulp build:sass`.

#### Available tasks:

`gulp build:less` - Starts a LESS workflow that proxies the dev server, keeping all your files locally, but visible in the browser. Refreshes CSS on save.

`gulp deploy:less` - Starts a LESS workflow that deploys your files automatically to the dev server on save.

Simply replace the above tasks with 'sass' to start SCSS workflows. (currently used for TWE Global)

You also have the flexibility of triggering a file sync manually via `gulp deploy`.

#### To do:

* Make the regEx task more flexible to intelligently load specified files (Images, Styles & Scripts).
* Convert to WebPack because it's 2016