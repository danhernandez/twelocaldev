/*
  ----------------------------- Paths Config
*/
var globalPaths = {
  //Global Variables
  atlasSite: 'Global',
  atlasDevURL: 'tweglobal-dev.twedigital.co'
}
var paths = {
  //General Path Config
  devSite: 'webdav-dev.twedigital.co',
  //The files to deploy
  mainCSS: '**.{map,css}',
  mainJS: '**.js',
  //WebDav Authentication
  //Not really sensitive so don't really care exposing these here,
  //but feel free to change these to your credentials
  authName: 'preprodazure.local\\daniel.rhm',
  authPass: 'Twepass@567',
  //The local paths
  localCSSRoot: '../' + globalPaths.atlasSite + '/TWE.' + globalPaths.atlasSite + '.UI/Includes/Core/LESS/' + globalPaths.atlasSite + '/',
  localJSRoot: '../' + globalPaths.atlasSite + '/TWE.' + globalPaths.atlasSite + '.UI/Includes/' + globalPaths.atlasSite + '/JS/',
  //The deploy paths
  deployCSSPath: '/Website/Includes/Core/LESS/',
  deployJSPath: '/Website/Includes/' + globalPaths.atlasSite + '/JS/'
}

/*
  ----------------------------- Required Resources
*/

var gulp = require('gulp'),
  browserSync = require('browser-sync'),
  reload = browserSync.reload,
  postcss = require('gulp-postcss'),
  sourcemaps = require('gulp-sourcemaps'),
  autoprefixer = require('autoprefixer'),
  gutil = require('gulp-util'),
  less = require('gulp-less'),
  sass = require('gulp-sass'),
  path = require('path'),
  minifyCSS = require('gulp-minify-css'),
  rename = require('gulp-rename'),
  webdav = require('gulp-webdav-sync'),
  //Optional Bourbon and Neat libs, not mandatory but enable if woring on twe-global
  bourbon = require('node-bourbon'),
  neat = require('node-neat').includePaths;

/*
  ----------------------------- Helper Utilities
*/

//prevents errors breaking watch
function swallowError(error) {
  console.log(error.toString());
  this.emit('end');
}

/*
  ----------------------------- Live Reload + inject styles via browser-sync
*/

/* Extreme hack alert: This task is purely for local devving only!!
  Hijacks network requests and rewrites to your local files, which injects back into your browser... don't forget to check into git to avoid work being lost!
*/
gulp.task('browsersync-inject', function() {
  browserSync({
    proxy: globalPaths.atlasDevURL,
    // notify: false, /* enable this to hide status notifications */
    // open: false, /* Enable this to prevent new browser window opening on build */
    rewriteRules: [{
      match: new RegExp('/Includes/Core/Less/Global/main.css'),
        fn: function() {
          return '/main.css'
        }
    }],
    files: '../Global/TWE.Global.UI/Includes/Core/LESS/Global/**.css',
    middleware: require('serve-static')('../Global/TWE.Global.UI/Includes/Core/LESS/Global'),
  });
});

// The 'normal' browsersync task!
gulp.task('browsersync', function() {
  browserSync({
    proxy: globalPaths.atlasDevURL
  })
});


/*
  ----------------------------- The LESS compile task
*/

//compiles LESS to CSS and minifies output to a new file
gulp.task('less', function() {
  return gulp.src([paths.localCSSRoot + '/**.less', '!/**/variables.less'])
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(gulp.dest(paths.localCSSRoot))
    .on('error', swallowError)
    .pipe(postcss([
      autoprefixer({
        browsers: ['last 2 versions', 'IE 9'],
        cascade: false
      })
    ]))
    .pipe(gulp.dest(paths.localCSSRoot))
    .pipe(minifyCSS({
      //skips '@import' statements in css
      processImport: false
    }))
    .pipe(rename('default.min.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.localCSSRoot))
    //Enable this for auto-browser reloading
    // .pipe(reload({
    //   stream: true
    // }));
});

/*
  ----------------------------- The SASS compile task
*/

gulp.task('sass', function() {
  //This'll probably need some better filtering options if project structures change
  return gulp.src([paths.localCSSRoot + '/**.scss'])
    .pipe(sourcemaps.init({
      loadMaps: true
    }))
    .pipe(sass({
      //listing node bourbon as a dependancy - turn this off if proj. doesn't require it
      includePaths: require('node-bourbon').includePaths,
      outputStyle: 'expanded'
    }))
    .pipe(gulp.dest(paths.localCSSRoot))
    .on('error', swallowError)
    .pipe(postcss([
      autoprefixer({
        browsers: ['last 2 versions', 'IE 9'],
        cascade: false
      })
    ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.localCSSRoot))
    .pipe(minifyCSS({
      //skips '@import' statements in css
      processImport: false
    }))
    .pipe(rename('main.min.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.localCSSRoot))
    //Enable this for auto-browser reloading
    // .pipe(reload({
    //   stream: true
    // }));
});

/*
  ----------------------------- The Deploy Task (web-dav-sync)
*/

//Deploys CSS
gulp.task('deployCSS', function(cb) {
  var webdavOptions = {
      protocol: 'http:',
      //auth: name:password
      auth: paths.authName + ':' + paths.authPass,
      hostname: paths.devSite,
      //default port
      port: 80,
      pathname: paths.deployCSSPath + globalPaths.atlasSite + '/',
      parent: paths.localCSSRoot,
      log: 'info',
      log: 'error',
      logAuth: true
    }
    //The root source CSS file/s
  return gulp.src([paths.localCSSRoot + '/**.css', paths.localCSSRoot + '/**.map'])
    .pipe(webdav(webdavOptions))
    .on('error', swallowError)
})

//Deploys JavaScript
gulp.task('deployJS', function() {
  var webdavOptions = {
      protocol: 'http:',
      //auth: name:password
      auth: paths.authName + ':' + paths.authPass,
      hostname: paths.devSite,
      //default port
      port: 80,
      pathname: paths.deployJSPath,
      parent: paths.localJSRoot,
      log: 'info',
      log: 'error',
      logAuth: true
    }
    //The root source JS file/s
  return gulp.src(paths.localJSRoot + paths.mainJS)
    .pipe(webdav(webdavOptions))
})

/*
  ----------------------------- The Task Pipelines
*/

// The overall deploy task - i.e deploys all files to webDAV
gulp.task('deploy', ['deployCSS', 'deployJS']);

/*
  LESS Development Stream
*/

// Compile LESS to CSS + DEPLOY latest files + reload the browser on updates
gulp.task('dev:less', ['less', 'deployCSS', 'watch:less'])
gulp.task('watch:less', function() {
  gulp.watch(paths.localCSSRoot + '/**.less', ['less', 'deployCSS']);
  gulp.watch(paths.localJSRoot + '/**.js', ['deployJS']);
});

gulp.task('watch:less:deploy', function() {
  gulp.watch(paths.localCSSRoot + '/**.less', ['less', 'deployCSS']);
  gulp.watch(paths.localJSRoot + '/**.js', ['deployJS']);
});

//end user tasks
gulp.task('deploy:less', ['dev:less', 'deployCSS', 'browsersync', 'watch:less:deploy']);
//OR reverse proxy the shit outta this and start a purely local dev environment
gulp.task('build:less', ['less', 'browsersync-inject', 'watch:less']);

/*
  SASS Development Stream
*/

// Compile SCSS to CSS + DEPLOY latest files + reload the browser on updates

gulp.task('dev:sass', ['sass', 'deployCSS', 'watch:sass'])
  //watch without deploying
gulp.task('watch:sass', function() {
  gulp.watch(paths.localCSSRoot + '/**.scss', ['sass']);
  gulp.watch(paths.localJSRoot + '/**.js', ['deployJS']);
});
//watch and deploy changes
gulp.task('watch:sass:deploy', function() {
  gulp.watch(paths.localCSSRoot + '/**.scss', ['sass']);
  gulp.watch(paths.localJSRoot + '/**.js', ['deployJS']);
});

//end user tasks
gulp.task('deploy:sass', ['dev:sass', 'deployCSS', 'browsersync', 'watch:sass:deploy'])
  //OR reverse proxy the shit outta this and start a purely local dev environment
gulp.task('build:sass', ['sass', 'browsersync-inject', 'watch:sass']);


//default task: deploy all files locally to webDAV server
gulp.task('default', ['deploy']);
